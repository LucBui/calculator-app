package calculator;

import java.util.Arrays;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class inFixtoPostFix {
    // Kiểm tra toán tử
    public boolean isOperator(char math) {
        char operator[] = { '+', '-', '*', '/' };
        Arrays.sort(operator);
        if (Arrays.binarySearch(operator, math) > -1)
            return true;
        else return false;
    }

    public boolean isOperatorParentheses(char mathOperator) {
        char operator [] = { '+', '-', '*', '/', '(', ')' };
        Arrays.sort(operator);
        if (Arrays.binarySearch(operator, mathOperator) > -1) {
            return true;
        } else return false;
    }

    //Thiết lập độ ưu tiên của các toán tử
    public int priority(char pri) {
        if (pri == '+' || pri == '-')
            return 1;
        else if (pri == '*' || pri == '/')
            return 2;
        else
            return 0;
    }

    // Kiểm tra chuỗi kí tự nhập vào
    public boolean checkCharacter(String charactor) {
        Pattern pattern = Pattern.compile("^[0-9\\+\\*\\-/()\\s]{1,}$");
        Matcher matcher = pattern.matcher(charactor);
        boolean match = matcher.matches();
        if (match == false) {
            System.out.println("Chuỗi ký tự nhập vào không đúng. Vui lòng thử lại ");
        }
        return match;
    }

    // Kiểm tra toán tử nhập vào
    public boolean checkOperator(String charactor) {
        boolean match = true;
        for (int i = 0; i < charactor.length(); i++) {
            int length = charactor.length();
            if (isOperator(charactor.charAt(length - 1)) == true) {
                match = false;
                System.out.println("Nhập thừa toán tử. Vui lòng thử lại");
                break;
            }
            char charactorOperation = charactor.charAt(i);
            if (isOperator(charactorOperation) == true) {
                char charactorOperation1 = charactor.charAt(i + 1);
                if (isOperator(charactorOperation1) == true || isOperator(charactor.charAt(0)) == true) {
                    match = false;
                    System.out.println("Nhập thừa toán từ. Vui lòng thử lại ");
                    break;
                }
            }
        }
        return match;
    }

    //Kiểm tra dấu ngoặc
    public  boolean checkParentheses(String charactor) {
        boolean count;
        boolean expression;
        boolean expressionOB = true;
        boolean expressionCB = true;
        boolean countOB = true;
        boolean countCB = true;
        char openBrackets = '(';
        int countLeft = 0;
        for(int i = 0; i < charactor.length(); i++) {
            if (charactor.charAt(i) == openBrackets) {
                countLeft++;
            }
        }
        for (int i = 0; i < charactor.length(); i++) {
            if (charactor.charAt(i) == openBrackets && charactor.indexOf("(") != charactor.length() - 1) {
                if (isOperator(charactor.charAt(i+1)) == true) {
                    expressionOB = false;
                    break;
                }
            }
        }
        char closeBrackets = ')';
        int countRight = 0;
        for (int i = 0; i < charactor.length(); i++) {
            if (charactor.charAt(i) == closeBrackets) {
                countRight++;
            }
        }
        for (int i = 0; i < charactor.length(); i++) {
            if (charactor.charAt(i) == closeBrackets && charactor.indexOf(")") != 0) {
                if (isOperator(charactor.charAt(i-1)) == true) {
                    expressionCB = false;
                    break;
                }
            }
        }
        if (countLeft == countRight) {
            count = true;
        }
        else {
            count = false;
        }
        for (int i = charactor.length()-1; i >= 0; i--) {
            if (charactor.charAt(i) == closeBrackets) {
                if(charactor.charAt(i) == '(') {
                    countOB = false;
                }
                break;
            }
            if (charactor.charAt(i) == openBrackets) {
                if (charactor.charAt(i) == ')') {
                    countOB = false;
                    break;
                }
            }
        }
        int indexOB = charactor.indexOf("(");
        int indexCB = charactor.indexOf(")");
        if ( indexCB < indexOB ) {
            countCB = false;
        }
        int check = charactor.indexOf(")(");
        if (check > 0) {
            countCB = false;
        }
        if(countOB == true && countCB == true && count == true && expressionOB == true && expressionCB == true) {
            expression = true;
        } else {
            expression = false;
            System.out.println("Kiểm tra lại dấu ngoặc");
        }
        return expression;
    }

    //Chuẩn hóa biểu thức nhập vào
    public String[] processString(String sMath) {
        String mathExpression = "", elementMath[] = null;
        inFixtoPostFix  IFP = new inFixtoPostFix();
        sMath = sMath.trim();
        sMath = sMath.replaceAll("\\s+"," ");
        for (int i = 0; i < sMath.length(); i++) {
            char mathOperator = sMath.charAt(i);
            if (!IFP.isOperatorParentheses(mathOperator))
                mathExpression = mathExpression + mathOperator;
            else mathExpression = mathExpression + " " + mathOperator + " ";
        }
        mathExpression = mathExpression.trim();
        mathExpression = mathExpression.replaceAll("\\s+"," ");
        elementMath = mathExpression.split(" ");
        return elementMath;
    }

    //Chuyển từ biểu thức trung tố sang hậu tố
    public String[] postFix(String[] elementMath) {
        inFixtoPostFix IFP = new inFixtoPostFix();
        String stringPostFix = "", E[];
        Stack<String> S = new Stack <String>();
        for (int i = 0; i < elementMath.length; i++) {
            char mathOperator = elementMath[i].charAt(0);
            if (!IFP.isOperatorParentheses(mathOperator))
                stringPostFix = stringPostFix + " " + elementMath[i];
            else {
                if (mathOperator == '(') S.push(elementMath[i]);
                else {
                    if (mathOperator == ')') {
                        char mathOperatorPostFix;
                        do {
                            mathOperatorPostFix = S.peek().charAt(0);
                            if (mathOperatorPostFix != '(') stringPostFix = stringPostFix + " " + S.peek();
                            S.pop();
                        } while (mathOperatorPostFix != '(');
                    }
                    else {
                        while (!S.isEmpty() && IFP.priority(S.peek().charAt(0)) >= IFP.priority(mathOperator)){
                            stringPostFix = stringPostFix + " " + S.peek();
                            S.pop();
                        }
                        S.push(elementMath[i]);
                    }
                }
            }
        }
        while (!S.isEmpty()) {
            stringPostFix = stringPostFix + " " + S.peek();
            S.pop();
        }
        E = stringPostFix.split(" ");
        return E;
    }

    // Chia cho 0
    public double divide(double num2, double num1) {
        if( num1 == 0 ) {
            System.out.println("Lỗi chia cho 0. Vui lòng nhập lại ");
        }
        return (double)num2/num1;
    }

    //Tính và cho ra kết quả chuỗi hậu tố
    public String valueMath(String[] stringPostFix) {
        Stack <String> S = new Stack<String>();
        inFixtoPostFix IFP = new inFixtoPostFix();
        for (int i = 1; i < stringPostFix.length; i++) {
            String string = stringPostFix[i];
            char c = stringPostFix[i].charAt(0);
            if (!IFP.isOperatorParentheses(c)) S.push(stringPostFix[i]);
            else {
                double num = 0;
                double num1 = Float.parseFloat(S.pop());
                double num2 = Float.parseFloat(S.pop());
                switch (c) {
                    case '+':
                        num = num2 + num1;
                        break;
                    case '-':
                        num = num2 - num1;
                        break;
                    case '*':
                        num = num2 * num1;
                        break;
                    case '/':
                        num = divide(num2, num1);
                        break;
                    default:
                        break;
                }
                S.push(Double.toString(num));
            }
        }
        return S.pop();
    }
}
