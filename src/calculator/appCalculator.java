package calculator;

import java.util.EmptyStackException;
import java.util.Scanner;

public class appCalculator {

    public static void main(String[] args) {
//        while (true) {
            inFixtoPostFix IFP = new inFixtoPostFix();
            Scanner scanner = new Scanner(System.in);
            System.out.print("Nhập vào biểu thức cần tính toán : ");
            String cal = scanner.nextLine();
            boolean conditionCharacter = IFP.checkCharacter(cal);
            boolean conditionOperator = IFP.checkOperator(cal);
            boolean conditionParentheses = IFP.checkParentheses(cal);
            if (conditionCharacter && conditionOperator && conditionParentheses) {
                String[] inFix = IFP.processString(cal);
                String[] postFix = IFP.postFix(inFix);
                try {
                    String ResultIN = IFP.valueMath(postFix);
                    System.out.println("Kết quả của biểu thức là: " + ResultIN);
                    System.out.println("---------------------------------");
                } catch (EmptyStackException exception) {
                    System.out.println("Không xác định giá trị biểu thức");
                }
            }
        }
//    }
}

