package calculatorTest;

import calculator.inFixtoPostFix;
//import org.junit.Test;
import org.testng.annotations.Test;

//import static org.junit.Assert.assertEquals;
import static org.testng.AssertJUnit.assertEquals;

public class appCalculatorTest {
    @Test
    public void conditionTest() {
        inFixtoPostFix IFP = new inFixtoPostFix();
        String cal = "10 + 2 * ( 3 + 4 ) - 6 / 2";
        boolean conditionCharacter = IFP.checkCharacter(cal);
        assertEquals(true, conditionCharacter);
        boolean conditionOperator = IFP.checkOperator(cal);
        assertEquals(true, conditionOperator);
        boolean conditionParentheses = IFP.checkParentheses(cal);
        assertEquals(true, conditionParentheses);
        if (conditionCharacter == true && conditionOperator == true && conditionParentheses == true) {
            String[] inFix = IFP.processString(cal);
            String[] postFix = IFP.postFix(inFix);
            String ResultIN = IFP.valueMath(postFix);
            assertEquals("21.0", ResultIN);
        }
    }
}
